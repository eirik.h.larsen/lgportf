# configuration settings

# data specific settings,  uncomment/ replace for new project
################################################################################

### six indicies
my_csv <- "Data/Indices_6.csv"
path_desc <- "Results/Indices_6/Descriptive"
path_fig <- "Results/Indices_6/Figures"
path_out <- "Results/Indices_6/Output"

# time series returns matrix
ts_start <- c(1980,2)
ts_end <- c(2018,8)
ts_freq <- 12
### end six indicies

### 12 industry portfolios
# https://mba.tuck.dartmouth.edu/pages/faculty/ken.french/Data_Library/det_17_ind_port.html
# 1M t-bill https://mba.tuck.dartmouth.edu/pages/faculty/ken.french/Data_Library/f-f_factors.html
# value weighted monthly data 
#my_csv <- "Data/12_Industry_Portfolios.CSV"
#path_desc <- "Results/Industry_12/Descriptive"
#path_fig <- "Results/Industry_12/Figures"
#path_out <- "Results/Industry_12/Output"

# time series returns matrix
#ts_start <- c(1955,1)
#ts_end <- c(2021,2)
#ts_freq <- 12
### end 12 industry portfolios


### 17 industry portfolios
# https://mba.tuck.dartmouth.edu/pages/faculty/ken.french/Data_Library/det_17_ind_port.html
# 1M t-bill https://mba.tuck.dartmouth.edu/pages/faculty/ken.french/Data_Library/f-f_factors.html
# value weighted monthly data 
#my_csv <- "Data/17_Industry_Portfolios.CSV"
#path_desc <- "Results/Industry_17/Descriptive"
#path_fig <- "Results/Industry_17/Figures"
#path_out <- "Results/Industry_17/Output"

# time series returns matrix
#ts_start <- c(1953,7)
#ts_end <- c(2021,2)
#ts_freq <- 12
### end 17 industry portfolios
################################################################################


# general settings, explore {bw_adj, win_tail, min_box, max_box} space
################################################################################

# quantile for defining bear market, applied to each asset separately
r_quantile <- c(0.05, 0.9)

# adjustment factor for lg_obj bandwidth selection: bw = sd*bw_adj
bw_adj = 1.1

# nobs from window tail used to predict next moving grid point (moving average)
# use win_tail = 1 for only last obs in window
win_tail <- 3

# box constraints for portfolio weights
min_box <- -0.5
max_box <- 1

# risk aversion parameter, quadratic utility
quad_util_par <- 0.5

# transactions costs
trans_cost <- 0.0015
################################################################################